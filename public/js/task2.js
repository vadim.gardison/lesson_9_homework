var renderList = (elem, title) => {
  var targetId = document.getElementById('target');
  var list = document.createElement('ul');
  var header = document.createElement('h3');
  list.appendChild(header);
  header.innerText = title;
  elem.forEach(item => {
    var listItem = document.createElement('li');
    listItem.innerText = item.name;
    list.appendChild(listItem);
  })
  list.appendChild(document.createElement('hr'));
  targetId.appendChild(list);
}

fetch("http://www.json-generator.com/api/json/get/coMGadIuJK?indent=2", {method: 'GET'}).then(
  res => {
    return res.json();
  }
).then(
  data => {
    let resultObj = {};
    let fruitCond = data.filter(item => {
      return item.favoriteFruit == 'banana'
    });
    let ageEyeColorCond = data.filter(item => {
      return item.age > 25 && item.eyeColor == 'brown';
    });
    let isAcEyeColBal = data.filter(item => {
      return item.isActive === false && item.eyeColor === 'blue' && item.balance.search(/^\$[1-9].+/) >= 0;
    })
    resultObj.fruitCond = fruitCond;
    resultObj.ageEyeColorCond = ageEyeColorCond;
    resultObj.isAcEyeColBal = isAcEyeColBal;
    renderList(resultObj.fruitCond, 'fruit = banana');
    renderList(resultObj.ageEyeColorCond, 'age>25, eyeColor === brown');
    renderList(resultObj.isAcEyeColBal, 'isActive === false, eyeColor === blue, balanse > 1000');
    console.log(resultObj);
  },
  err => {
    console.log('error');
  }
)